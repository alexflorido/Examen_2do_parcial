-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-05-2018 a las 06:23:40
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `examen_progra`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `id_administrador` int(12) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `ap_pat` varchar(30) DEFAULT NULL,
  `ap_mat` varchar(30) DEFAULT NULL,
  `ci` int(11) NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `clave` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id_administrador`, `nombre`, `ap_pat`, `ap_mat`, `ci`, `usuario`, `clave`) VALUES
(123, 'Alexander', 'Florido', 'Siles', 5299072, 'admin', '1234');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int(12) NOT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `descipcion` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `nombre`, `descipcion`) VALUES
(1, 'False/Verdadero', 'El evaluado debe elejir si la frase es correcta o incorrecta'),
(2, 'Seleccion Multiple', 'El evaluado debera elejir la respuesta correcta'),
(3, 'Completado la Frase', 'El evaluado debera completar con la palabra correcta la frase');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiante`
--

CREATE TABLE `estudiante` (
  `id_estudiante` int(12) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `ap_pat` varchar(30) DEFAULT NULL,
  `ap_mat` varchar(30) DEFAULT NULL,
  `ci` int(11) NOT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `usuario` varchar(20) NOT NULL,
  `clave` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estudiante`
--

INSERT INTO `estudiante` (`id_estudiante`, `nombre`, `ap_pat`, `ap_mat`, `ci`, `fecha_nacimiento`, `usuario`, `clave`) VALUES
(1, 'alexander', 'Florido', 'siles', 1234, '2010-05-18', 'user', 'hola');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen`
--

CREATE TABLE `examen` (
  `id_examen` int(12) NOT NULL,
  `nota_total` float DEFAULT NULL,
  `id_materia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `examen`
--

INSERT INTO `examen` (`id_examen`, `nota_total`, `id_materia`) VALUES
(1, 100, 1),
(3, 100, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen_estudiante`
--

CREATE TABLE `examen_estudiante` (
  `id_examen_estudiante` int(12) NOT NULL,
  `id_examen` int(11) NOT NULL,
  `id_estudiante` int(11) DEFAULT NULL,
  `nota` float NOT NULL,
  `hora_inicio` varchar(30) NOT NULL,
  `hora_fin` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `examen_estudiante`
--

INSERT INTO `examen_estudiante` (`id_examen_estudiante`, `id_examen`, `id_estudiante`, `nota`, `hora_inicio`, `hora_fin`) VALUES
(1, 1, 1, 0, '05:23:27', '05:25:30'),
(2, 1, 1, 75, '05:26:55', '05:27:32'),
(3, 1, 1, 50, '05:43:55', '05:44:05'),
(4, 1, 1, 50, '05:43:55', '05:46:35'),
(5, 1, 1, 0, '05:52:23', '05:54:42'),
(6, 1, 1, 0, '06:01:51', '06:03:23'),
(37, 3, 1, 0, '12:12:57', '12:12:58'),
(38, 3, 1, 50, '12:14:32', '12:14:36'),
(39, 3, 1, 0, '12:20:39', '12:20:40'),
(40, 3, 1, 100, '12:21:08', '12:21:14'),
(41, 3, 1, 0, '12:22:13', '12:22:14'),
(42, 3, 1, 100, '12:22:36', '12:22:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia`
--

CREATE TABLE `materia` (
  `id_materia` int(12) NOT NULL,
  `nombre_materia` varchar(30) NOT NULL,
  `descripcion` varchar(10000) DEFAULT NULL,
  `id_administrador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `materia`
--

INSERT INTO `materia` (`id_materia`, `nombre_materia`, `descripcion`, `id_administrador`) VALUES
(1, 'Calculo I', 'En general el término cálculo (del latín calculus = piedra)1? hace referencia al resultado correspon', 123),
(2, 'Programacion', 'Linda Programacion', 123);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nota`
--

CREATE TABLE `nota` (
  `id_nota` int(12) NOT NULL,
  `total` float NOT NULL,
  `id_examen` int(11) NOT NULL,
  `id_estudiante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nota`
--

INSERT INTO `nota` (`id_nota`, `total`, `id_examen`, `id_estudiante`) VALUES
(1, 0, 3, 1),
(2, 100, 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pregunta`
--

CREATE TABLE `pregunta` (
  `id_pregunta` int(12) NOT NULL,
  `txt_pregunta` varchar(200) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `respuesta` varchar(30) NOT NULL,
  `puntaje` float NOT NULL,
  `id_administrador` int(11) NOT NULL,
  `id_examen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pregunta`
--

INSERT INTO `pregunta` (`id_pregunta`, `txt_pregunta`, `id_categoria`, `respuesta`, `puntaje`, `id_administrador`, `id_examen`) VALUES
(1, '2+2 es igual a 25', 1, 'falso', 25, 123, 1),
(2, 'Resualva la Siguiente Operacion, 2x4 es igual a: A) 24; B) 8; C) 10', 2, 'B', 50, 123, 1),
(3, 'El simbolo + hace referencia a una:', 3, 'suma', 25, 123, 1),
(4, 'Esta Es la Pregunta Definitiva', 2, 'Esta es la Respuesta Definitiv', 4, 123, 1),
(5, 'Una variable de tipo entera es String', 1, 'falso', 50, 123, 3),
(6, 'Una variable de tipo cadena es string', 1, 'verdadero', 50, 123, 3);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id_administrador`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `estudiante`
--
ALTER TABLE `estudiante`
  ADD PRIMARY KEY (`id_estudiante`);

--
-- Indices de la tabla `examen`
--
ALTER TABLE `examen`
  ADD PRIMARY KEY (`id_examen`);

--
-- Indices de la tabla `examen_estudiante`
--
ALTER TABLE `examen_estudiante`
  ADD PRIMARY KEY (`id_examen_estudiante`);

--
-- Indices de la tabla `materia`
--
ALTER TABLE `materia`
  ADD PRIMARY KEY (`id_materia`);

--
-- Indices de la tabla `nota`
--
ALTER TABLE `nota`
  ADD PRIMARY KEY (`id_nota`);

--
-- Indices de la tabla `pregunta`
--
ALTER TABLE `pregunta`
  ADD PRIMARY KEY (`id_pregunta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id_administrador` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT de la tabla `estudiante`
--
ALTER TABLE `estudiante`
  MODIFY `id_estudiante` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `examen`
--
ALTER TABLE `examen`
  MODIFY `id_examen` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `examen_estudiante`
--
ALTER TABLE `examen_estudiante`
  MODIFY `id_examen_estudiante` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `materia`
--
ALTER TABLE `materia`
  MODIFY `id_materia` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `nota`
--
ALTER TABLE `nota`
  MODIFY `id_nota` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `pregunta`
--
ALTER TABLE `pregunta`
  MODIFY `id_pregunta` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
